# Station F

## Usage

```
npm install
npm run install-client
```

and then on 2 terminals,

for the server :

```
npm run dev
```

for the client :

```
npm run client
```

And access it from http://localhost:8080/ or directly from hosted app : https://stationf-oozkaya.herokuapp.com/