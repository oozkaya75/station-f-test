const mongoose = require('mongoose');
const isNotBooked = require("../helpers/isNotBooked");

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const bookingSchema = new mongoose.Schema({
	roomId: {
		type: ObjectId,
		required: [true, 'Room Id is required']
	},
	date: {
		type: Date,
		required: [true, 'Date is required']
	}
});

const Booking = mongoose.model("booking", bookingSchema);

const getBookings = async () => {
	return await Booking.find();
};

const createBooking = async data => {
	const { roomId, date } = data;
	const room = { _id: mongoose.Types.ObjectId(roomId) };
	const bookings = await getBookings();

	if (!isNotBooked(room, bookings, date))
		throw new Error("Salle deja réservée");

	return new Booking({
		roomId,
		date,
	}).save();
};

const findBooking = async roomName => {
	return await Booking.findOne({ roomName });
};

module.exports = {
	getBookings,
	createBooking,
	findBooking
};