const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Room name is required']
    },
    description: {
        type: String,
        required: [true, 'Description is required']
    },
    capacity: {
        type: Number,
        required: [true, 'Capacity is required']
    },
    equipements: {
        type: Array,
        default: []
    },
    createdAt: {
        type: Date,
        required: [true, 'Created date is required']
    },
    updatedAt: {
        type: Date,
        required: [true, 'Updated date is required']
    }
});

const Room = mongoose.model("room", roomSchema);

const getRooms = async () => {
    return await Room.find();
};

const createRoom = async data => {
    const room = await findRoom(data.name);
    if (room)
        return;

    const { name, description, capacity, equipements, createdAt, updatedAt } = data
    return new Room({
        name,
        description,
        capacity,
        equipements,
        createdAt,
        updatedAt
    }).save();
};

const findRoom = async name => {
    return await Room.findOne({ name });
};

module.exports = {
    getRooms,
    createRoom,
    findRoom
};