const mongoose = require('mongoose');

const connectionString = process.env.MONGODB_URL || "mongodb+srv://root:1234@cluster0.2f1xc.mongodb.net/stationf?retryWrites=true&w=majority"

mongoose.connect(
    connectionString,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

const db = mongoose.connection;

module.exports = db;