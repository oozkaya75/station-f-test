const isNotBooked = (room, bookings, date) => {
    for (booking of bookings) {
        if (!room._id.equals(booking.roomId))
            continue;
        let bookingStart = new Date(booking.date).getTime();
        let bookingEnd = bookingStart + 3600000 // + 1 hour in miliseconds
        let requestStart = new Date(date).getTime();
        let requestEnd = requestStart + 3600000;

        let startIsNotValid = requestStart >= bookingStart && requestStart < bookingEnd;
        let endIsNotValid = requestEnd < bookingEnd && requestEnd > bookingStart;
        if (startIsNotValid || endIsNotValid)
            return false;
    }

    return true;
};

module.exports = isNotBooked;