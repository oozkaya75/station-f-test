const RoomLib = require("../mongoose/room");
const BookingLib = require("../mongoose/booking");
const isNotBooked = require("./isNotBooked");

const hasCapacity = (room, capacity) => {
    return room.capacity >= capacity
};

const hasEquipements = (room, equipements) => {
    const len = equipements.length;

    if (!len)
        return true;
    let counter = 0;
    for (equipement of equipements) {
        if (room.equipements.some(eq => eq["name"] == equipement))
            counter++;
    }

    return counter == len;
};

// Calculations are done assuming a booking is done for 1 hour
const getAvailableRooms = async req => {
    const { date, capacity, equipements } = req.body;
    const rooms = await RoomLib.getRooms();
    const bookings = await BookingLib.getBookings();

    const availableRooms = rooms.filter(room => {
        if (isNotBooked(room, bookings, date) && hasCapacity(room, capacity) && hasEquipements(room, equipements))
            return true;
        return false;
    });

    return availableRooms;
};

module.exports = getAvailableRooms;