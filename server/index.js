const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const request = require("request");

const RoomLib = require("./mongoose/room");
const BookingLib = require("./mongoose/booking");

const getAvailableRooms = require("./helpers/getAvailableRooms")

const db = require("./mongoose");
const app = express();

db.once("open", () => {
    console.log("Database connected !")
    const url = { url: "https://online.stationf.co/tests/rooms.json", json: true };
    request(url, (error, res, body) => {
        if (!error && res.statusCode == 200) {
            body.rooms.forEach(room => {
                RoomLib.createRoom(room)
            });
        }
    });
})

db.on("error", err => {
    console.error("connection error:", err)
})

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/rooms", async (req, res) => {
    const rooms = await RoomLib.getRooms();
    res.send(rooms);
});
app.post("/available-rooms", async (req, res) => {
    const availableRooms = await getAvailableRooms(req);
    res.send(availableRooms);
});
app.post("/booking", async (req, res) => {
    try {
        await BookingLib.createBooking(req.body);
        res.send("OK");
    } catch (err) {
        console.log(err.message)
        res.status(400).send(err.message);
    }
});

if (process.env.NODE_ENV === "production") {
    app.use(express.static("client/dist"));
}

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});